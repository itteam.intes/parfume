import { MemberComment, Product } from "@types";

export const productPrice = [
  { name: "0€ — 10€", value: "0-10" },
  { name: "11€ — 20€", value: "11-20" },
  { name: "21€ — 50€", value: "21-50" },
];
export const productFilter = [
  { name: "Trier Par...", value: "" },
  { name: "Alphabétique (A à Z)", value: "alpha-asc" },
  { name: "Alphabétique (Z à A)", value: "alpha-desc" },
  { name: "Prix Croissant", value: "price-asc" },
  { name: "Prix Décroissant", value: "price-desc" },
  { name: "Nombres D'avis", value: "evaluate" },
];
export const colourOptions = [
  { value: "orange", label: "Orange" },
  { value: "yellow", label: "Yellow" },
  { value: "green", label: "Green" },
  { value: "forest", label: "Forest" },
  { value: "slate", label: "Slate" },
  { value: "silver", label: "Silver" },
];



export const featuredComments: MemberComment[] = [
  {
    id: "1",
    member: "MARIELLE",
    text: "Très contente, texture agréable, excellent résultat pour une peau très douce et bien nourrie ! Pratique et très joli, en plus la créatrice est adorable.",
  },
  {
    id: "2",
    member: "SOPHIE",
    text: "Étant depuis un certain temps à la recherche de produits efficaces pour ma peau, vous devez imaginer comment je me suis sentie quand je suis tombée sur Nature Féerique qui non seulement m’a donné totale satisfaction, et en plus offre des produits 100% naturels. Je dirais juste deux mots : « J’adore ! ».",
  },
  {
    id: "3",
    member: "JULIE",
    text: "Les produits Nature Féerique sont idéaux pour toute personne ayant la peau sensible comme moi.",
  },
  {
    id: "4",
    member: "STÉPHANIE",
    text: "Ça fait déjà quelques temps que j’utilise cette marque de produits. Et je trouve qu’elle offre d'excellents produits qualités/prix et c’est un superbe cadeau à offrir à vos proches. STÉPHANIE",
  },
  {
    id: "5",
    member: "LUCCI",
    text: "Mon mari m’a offert le baume Précieux lèvres et mains et le baume soin précieux. Je peux vous dire que ces produits sont justes magiques. La sensation que vous avez après usage est incroyable.",
  },
];

export const homeSlideInfo = [
  {
    url: "https://naturefeerique.fr/wp-content/uploads/2022/03/slide1-1-scaled-1.jpg",
    caption: "",
    text: "J'ADORE",
    href: "https://nature-feerique.sumupstore.com/",
  },
  {
    url: "https://naturefeerique.fr/wp-content/uploads/2022/03/s3-1.png",
    text: "Je Sucummbe",
    caption: "",
    href: "https://nature-feerique.sumupstore.com/",
  },
  {
    url: "https://naturefeerique.fr/wp-content/uploads/2022/03/mainof.jpg",
    text: "OUI",
    caption: "",
    href: "https://nature-feerique.sumupstore.com/",
  },
];

export const TextNature = [
  "Il y a 3 ans, en organisant les valises pour partir en vacances en famille, j’ai constaté que ma salle de bain regorgeait de produits d’hygiène & soin en tout genre : eau micellaire, lait démaquillant, démaquillant yeux, nettoyant visage, crème à raser, crème hydratante visage, gel après-rasage, crème corps, déodorant femme, déodorant homme, gel douche, gel douche kids, shampooing, shampooing kids, après-shampooing, démêlant cheveux, masque cheveux… Le constat est alarmant : nous utilisons quotidiennement des produits cosmétiques similaires, volumineux, composés avec 80 à 85% d’eau, contenus dans des bouteilles plastiques encombrantes et polluantes. Chaque cosmétique ayant sa fonction et/ou sa cible multiplie considérablement notre consommation et nos déchets. A ce moment précis, j’ai imaginé créer une gamme raisonnable et responsable, adaptée à toute la famille. Des produits solides pour une utilisation facile, nomade et économique ; aux formules minimalistes sans eau donc sans conservateur pour des cosmétiques plus concentrés et sains ; avec des ingrédients naturels et principalement locaux dans le respect de l’environnement. L’aventure a commencé en novembre 2019, avec la création de l’identité de la marque Nature Féerique, son enregistrement puis la rencontre avec les fournisseurs soutenant le projet. Deux ans après, de la recherche des meilleurs ingrédients, majoritairement locaux, au développement des formules, en passant par des tests et ajustements pour trouver la texture idéale, Nature Féerique est fière de dévoiler ces 3 premiers produits cosmétiques : - Déodorant Plume, un soin déodorant naturel solide - Démaquillant Cocoon, une huile solide 2 en 1 : nettoyante et démaquillante - Soin Précieux, une crème solide multi-usages Nature Féerique a eu le privilège de promouvoir le lancement de ces cosmétiques solides au sein du Comptoir des artisans dans le magasin alinea Avignon-Le Pontet, nouveau lieu éphémère d’échanges et de partages de l’enseigne alinea. Dans sa démarche, Nature Féerique s’engage à accompagner les familles vers une routine beauté minimaliste et une consommation cosmétique plus responsable.",
];
export const TextCreatrice = [
  "Née à Beaune (Côte d’Or), j’ai grandi au milieu des plus beaux vignobles de Bourgogne. C’est vers l’âge de 7 ans que j’ai commencé à me passionner pour les produits de beauté à travers une collection de miniatures de parfum. D’une imagination débordante, j’ai expérimenté mes premiers mélanges avec du gros sel, des craies de couleur et quelques gouttes de parfum dans un bocal en verre pour créer des sels de bain ; auxquels je pouvais rajouter du savon liquide. De caractère déterminée, c’est à la fin du collège, que je me suis orientée vers la chimie.",
  "Durant mes années Lycéennes, j’ai rencontré l’homme extraordinaire, avec qui je partage ma vie aujourd’hui. Ensemble, nous sommes partis dans le Sud (Toulouse et Montpellier) pour réaliser nos études universitaires.",
  "Ma formation ciblée en chimie, cosmétique et esthétique m’a ainsi permis d’acquérir une double compétence : formulation cosmétique et technique esthétique. J’ai donc une parfaite maîtrise quant à la création de produits cosmétiques en adéquation avec leur utilisation et les besoins des différents types de peau.",
  "Au cours de ma Licence professionnelle Parfums, Arômes et Cosmétiques, j’ai pris énormément de plaisir à réaliser un projet tutoré présenté au Concours national U’Cosmetics pour lequel j’ai obtenu fièrement le 1er prix du « concept cosmétique Innovant ». C’est l’accomplissement de ce projet qui m’a donné l’envie de développer une gamme de produits cosmétiques.",
  "Diplômés dans nos domaines respectifs, nos premiers contrats nous ont mené en Drôme Provençale, où nous résidons aujourd’hui avec nos deux adorables filles.",
  "Etant comblée familialement, et ayant pu acquérir d’un point de vue professionnel un savoir-faire maîtrisé (7 années riches d’une expérience au sein d’un laboratoire de recherche et développement à créer des formules cosmétiques pour les plus grandes marques), j’ai décidé de réaliser ce rêve de jeune diplômée qui me tenait à cœur : créer ma propre marque de cosmétiques 100% naturels, à la fois efficaces et sains pour la santé dans le respect de l’environnement !",
  "A travers ces quelques lignes, je tiens tout d’abord à vous raconter ma petite histoire, et par la même occasion vous exprimer ma reconnaissance pour votre soutien depuis la création de la marque « Nature Féerique ». Merci infiniment pour votre confiance et votre fidélité.",
];

export const BriefTextNature = [
  "Il y a 3 ans, en organisant les valises pour partir en vacances en famille, j’ai constaté que ma salle de bain regorgeait de produits d’hygiène & soin en tout genre : eau micellaire, lait démaquillant, démaquillant yeux, nettoyant visage, crème à raser, crème hydratante visage, gel après-rasage, crème corps, déodorant femme, déodorant homme, gel douche, gel douche kids, shampooing, shampooing kids, après-shampooing, démêlant cheveux, masque cheveux…",
];
export const BriefTextCreatrice = [
  "Née à Beaune (Côte d’Or), j’ai grandi au milieu des plus beaux vignobles de Bourgogne. C’est vers l’âge de 7 ans que j’ai commencé à me passionner pour les produits de beauté à travers une collection de miniatures de parfum. D’une imagination débordante, j’ai expérimenté mes premiers mélanges avec du gros sel, des craies de couleur et quelques gouttes de parfum dans un bocal en verre pour créer des sels de bain ; auxquels je pouvais rajouter du savon liquide. De caractère déterminée, c’est à la fin du collège, que je me suis orientée vers la chimie.",
];

export const blog = [
  {
    id: 1,
    title: "Marché de Noël virtuel",
    subTitle: "LIRE LA SUITE",
    image: "/images/blog1.webp",
    date: "november 15, 2021",
  },
  {
    id: 2,
    title: "Le Comptoir D’Alinea",
    subTitle: "LIRE LA SUITE",
    image: "/images/blog.webp",
    date: "septembre 15, 2021",
  },
];
